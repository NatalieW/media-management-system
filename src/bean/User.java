package bean;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="User")
public class User implements Serializable{

	/**
	 * User class for registering a user in the database
	 */
	private static final long serialVersionUID = 1L;
	private String username;
	private String email;
	private String password;
	
	public User() {
		
	}
	
	public User(String username, String email, String password) 
	{
		this.username = username;
		this.email = email;
		this.password = password;
		
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}

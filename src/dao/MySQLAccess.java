package dao;

/**
 * This class provides the methods for adding a new item or update own in the db
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MySQLAccess {
	private static Connection connect = null;

	// Define Hostname
	private static String dbHost = "172.16.176.58";

	// Port- Standard : 3306
	private static String dbPort = "3306";

	// Define database
	private static String database = "media";

	// Database user
	private static String dbUser = "sqlUser";

	// Database pw
	private static String dbPassword = "mysqlpw";

	public void readDataBase() throws Exception {
		try {
			// this will load the MySQL driver, each DB has its own driver
			Class.forName("com.mysql.jdbc.Driver");
			// setup the connection with the DB.
			connect = DriverManager.getConnection("jdbc:mysql://" + dbHost
					+ ":" + dbPort + "/" + database + "?" + "user=" + dbUser
					+ "&" + "password=" + dbPassword);

		} catch (ClassNotFoundException e) {
			System.out.println("Treiber nicht gefunden");
		} catch (SQLException e) {
			System.out.println("Connect nicht moeglich");
		}
	}

	private static Connection getInstance() {
		if (connect == null)
			new MySQLAccess();
		return connect;
	}

	/**
	 * Schreibt die Namensliste in die Konsole
	 */
	public static void printNameList() {
		connect = getInstance();

		if (connect != null) {
			// Anfrage-Statement erzeugen.
			Statement query;
			try {
				query = connect.createStatement();

				// Ergebnistabelle erzeugen und abholen.
				String sql = "SELECT title, author FROM media "
						+ "ORDER BY id";
				ResultSet result = query.executeQuery(sql);

				// Ergebniss�tze durchfahren.
				while (result.next()) {
					String title = result.getString("title"); // Alternativ:
																		// result.getString(1);
					String author = result.getString("author"); // Alternativ:
																		// result.getString(2);
					System.out.println(title);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * F�gt einen neuen Datensatz hinzu
	 */
	public static void insertMedia(String title, String author) {
		connect = getInstance();

		if (connect != null) {
			try {

				// Insert-Statement erzeugen (Fragezeichen werden sp�ter
				// ersetzt).
				String sql = "INSERT INTO media(title, author) "
						+ "VALUES(?, ?)";
				PreparedStatement preparedStatement = connect
						.prepareStatement(sql);
				// Erstes Fragezeichen durch "firstName" Parameter ersetzen
				preparedStatement.setString(1, title);
				// Zweites Fragezeichen durch "lastName" Parameter ersetzen
				preparedStatement.setString(2, author);
				// SQL ausf�hren.
				preparedStatement.executeUpdate();

				// Es wird der letzte Datensatz abgefragt
				String lastActor = "SELECT id, title, author "
						+ "FROM media " + "ORDER BY id DESC LIMIT 1";
				ResultSet result = preparedStatement.executeQuery(lastActor);

				// Wenn ein Datensatz gefunden wurde, wird auf diesen
				// zugegriffen
				if (result.next()) {
					System.out.println("(" + result.getInt(1) + ")"
							+ result.getString(2) + " " + result.getString(3));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Aktualisiert den Datensatz mit der �bergebenen actorId
	 */
	public static void updateMedia(String title, String author, int id) {
		connect = getInstance();

		if (connect != null) {
			try {

				String querySql = "SELECT id, title, author "
						+ "FROM book " + "WHERE id = ?";

				// PreparedStatement erzeugen.
				PreparedStatement preparedQueryStatement = connect
						.prepareStatement(querySql);
				preparedQueryStatement.setInt(1, id);
				ResultSet result = preparedQueryStatement.executeQuery();

				if (result.next()) {
					// Vorher
					System.out.println("VORHER: (" + result.getInt(1) + ")"
							+ result.getString(2) + " " + result.getString(3));
				}

				// Ergebnistabelle erzeugen und abholen.
				String updateSql = "UPDATE media "
						+ "SET title = ?, author = ? "
						+ "WHERE id = ?";
				PreparedStatement preparedUpdateStatement = connect
						.prepareStatement(updateSql);
				// Erstes Fragezeichen durch "firstName" Parameter ersetzen
				preparedUpdateStatement.setString(1, title);
				// Zweites Fragezeichen durch "lastName" Parameter ersetzen
				preparedUpdateStatement.setString(2, author);
				// Drittes Fragezeichen durch "actorId" Parameter ersetzen
				preparedUpdateStatement.setInt(3, id);
				// SQL ausf�hren
				preparedUpdateStatement.executeUpdate();

				// Es wird der letzte Datensatz abgefragt
				result = preparedQueryStatement.executeQuery();

				if (result.next()) {
					System.out.println("NACHHER: (" + result.getInt(1) + ")"
							+ result.getString(2) + " " + result.getString(3));
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
package menue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import dao.MySQLAccess;

@ManagedBean (name="menueView")
public class MenueView {

	private String mediatype;

	public String add() throws IOException {
		MySQLAccess.insertMedia("Der Hundertjährige", "Göteborg");
		addMessage("Success", "Data saved");
		return "addMedia.xhtml";
	}

	public void update() {
		MySQLAccess.updateMedia("", "", 3);
		addMessage("Success", "Data updated");
	}

	public void delete() {
		addMessage("Success", "Data deleted");
	}

	public void addMessage(String summary, String detail) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,
				summary, detail);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public String getMediaType() {
		return mediatype;
	}

	public void setMediaType(String mediaType) {
		this.mediatype = mediaType;
	}
}
